from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404
from .models import Type, Material, Loan, LoanMaterial, UserProfile
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, logout, authenticate
from .forms import NewUserForm, TypeForm, MaterialForm, LoanerForm, LoanForm
from .models import Material, Type
from django.contrib import messages
from django.core.paginator import Paginator
from django.views.generic import ListView, DetailView, View
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist


# Create your views here.
def homepage(request):
    materials = Material.objects.all()

    search_term = ""

    if 'search' in request.GET:
        search_term = request.GET['search']
        materials = (materials.filter(name__icontains=search_term) | materials.filter(barcode__icontains=search_term))

        if search_term == "":
            search_term = "No match."

    paginator = Paginator(materials, 8)

    page = request.GET.get('page')

    materials = paginator.get_page(page)

    return render(request=request,
                  template_name="main/home.html",
                  context={"materials": materials, "search_term": search_term})


class MaterialDetailView(DetailView):
    model = Material
    template_name = "main/material.html"


def register(request):
    # automatically, it's a GET request

    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            # things are filled out like theyre supposed to
            user = form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"New Account Created:{username}")
            login(request, user)
            messages.info(request, f"Logged in as  {username}")
            return redirect("main:homepage")
        else:
            for message in form.error_messages:
                # print(form.error_messages[message])
                messages.error(request, f"{message} : {form.error_messages[message]}")
    form = NewUserForm
    return render(request,
                  "main/register.html",
                  context={"form": form}
                  )


def logout_request(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect("main:homepage")


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"Logged in as  {username}")
                return redirect("main:homepage")
            else:
                messages.error(request, "Invalid username or password")
        else:
            messages.error(request, "Invalid username or password")

    form = AuthenticationForm()
    return render(request,
                  "main/login.html",
                  {"form": form}
                  )


def material_form(request):
    sauvegarde = False
    if request.method == 'POST':
        form = MaterialForm(request.POST, request.FILES)
        if form.is_valid():
            from django.core.exceptions import ObjectDoesNotExist
            try:
                material_unique = form.cleaned_data.get('type')
                Material.objects.get(type=material_unique)
                messages.error(request, f"Existing unique type.")
            except ObjectDoesNotExist:
                form.save()
                form = MaterialForm()
                messages.success(request, f"New Material created")
                return redirect("main:homepage")
                sauvegarde = True
    else:
        form = MaterialForm()
    return render(request, 'main/materialForm.html', {'form': form, 'sauvegarde': sauvegarde})


def type_form(request):
    sauvegarde = False
    if request.method == 'POST':
        form = TypeForm(request.POST)
        form.save()
        form = TypeForm()
        messages.success(request, f"New Type created")
        return redirect("main:homepage")
        sauvegarde = True
    else:
        form = TypeForm()
    return render(request, 'main/typeForm.html/', {'form': form, 'sauvegarde': sauvegarde})


def loaner_form(request):
    sauvegarde = False
    if request.method == 'POST':
        form = LoanerForm(request.POST)
        form.save()
        form = LoanerForm()
        messages.success(request, f"New Loaner created")
        return redirect("main:homepage")
        sauvegarde = True
    else:
        form = LoanerForm()
    return render(request, 'main/loanerForm.html/', {'form': form, 'sauvegarde': sauvegarde})


@login_required
def add_to_loan(request, slug):
    material = get_object_or_404(Material, slug=slug)
    loan_material, created = LoanMaterial.objects.get_or_create(material=material, user=request.user, ordered=False)
    material_query = Loan.objects.filter(user=request.user, ordered=False)
    if material_query.exists():
        loan = material_query[0]
        # check if the order item is in the order
        if loan.materials.filter(material__slug=material.slug).exists():
            loan_material.quantity += 1
            loan_material.save()
            messages.info(request, loan_material)
            return redirect("main:material", slug=slug)
        else:
            loan.materials.add(loan_material)
            messages.info(request, loan_material)
            return redirect("main:material", slug=slug)

    else:
        loan_date = timezone.now()
        loan = Loan.objects.create(user=request.user, creation_date_loan=loan_date)
        loan.materials.add(loan_material)
        messages.info(request, "This item was added to your cart.")
        return redirect("main:material", slug=slug)


@login_required
def remove_from_loan(request, slug):
    material = get_object_or_404(Material, slug=slug)
    material_query = Loan.objects.filter(user=request.user, ordered=False)

    if material_query.exists():
        loan = material_query[0]
        # check if the order item is in the order
        if loan.materials.filter(material__slug=material.slug).exists():
            loan_material = LoanMaterial.objects.filter(material=material, user=request.user, ordered=False)[0]
            loan.materials.remove(loan_material)
            loan_material.delete()
            messages.info(request, "This item was removed from your cart.")
            return redirect("main:loan-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("main:material", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("main:material", slug=slug)


@login_required
def remove_single_item_from_loan(request, slug):
    material = get_object_or_404(Material, slug=slug)
    material_query = Loan.objects.filter(user=request.user, ordered=False)
    if material_query.exists():
        loan = material_query[0]
        # check if the order item is in the order
        if loan.materials.filter(material__slug=material.slug).exists():
            loan_material = LoanMaterial.objects.filter(
                material=material,
                user=request.user,
                ordered=False)[0]
            if loan_material.quantity > 1:
                loan_material.quantity -= 1
                loan_material.save()
            else:
                loan.materials.remove(loan_material)
            messages.info(request, "This item quantity was updated.")
            return redirect("main:loan-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("main:material", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("main:material", slug=slug)


class LoanSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            loan = Loan.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': loan
            }
            return render(self.request, 'main/loan_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")


class loan_form(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            form = LoanForm()
            context = {
                'formLoan': form,
            }
            return render(self.request, 'main/loanForm.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")

    def post(self, *args, **kwargs):
        form = LoanForm(self.request.POST or None)
        loan = Loan.objects.get(user=self.request.user, ordered=False)
        try:
            if form.is_valid():
                expected_return = form.cleaned_data.get('expected_return_date')

                loan.ordered = True
                loan.save()

                loan.expected_return_date = expected_return
                loan.save()

                loan.loaner = form.cleaned_data.get('loaner')
                loan.save()

                loan.user = self.request.user
                loan.save()

                loan_materials = loan.materials.all()
                loan_materials.update(ordered=True)
                for material in loan_materials:
                    material.save()

                messages.success(self.request, f" Loan saved successfully")
                return redirect("main:homepage")
            else:
                messages.info(self.request, "Please fill in the required fields")
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active loan")
        return redirect("main:loan-summary")

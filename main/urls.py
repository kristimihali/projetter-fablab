"""ProjetTutoreFabMSTIC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls.static import static

from .views import(
    MaterialDetailView,
    LoanSummaryView,
    loan_form,
)

app_name = 'main'  # here for namespacing of urls.

urlpatterns = [
                  # path('admin/', admin.site.urls),
                  path("", views.homepage, name="homepage"),
                  path("register/", views.register, name="register"),
                  path("logout/", views.logout_request, name="logout"),
                  path("login/", views.login_request, name="login"),
                  path("loan-summary/", LoanSummaryView.as_view(), name="loan-summary"),
                  path("material/", views.material_form, name="material"),
                  path("type/", views.type_form, name="type"),
                  path("loaner/", views.loaner_form, name="loaner"),
                  path("loan/", loan_form.as_view(), name="loan"),

                  path("material/<slug>/", MaterialDetailView.as_view(), name='material'),
                  path("add-to-loan/<slug>", views.add_to_loan, name='add-to-loan'),
                  path("remove-from-loan/<slug>", views.remove_from_loan, name='remove-from-loan'),
                  path("remove-material-from-loan/<slug>", views.remove_single_item_from_loan, name='remove-material-from-loan'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from random import choices
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Material, Type, Loan, LoanMaterial, Loaner
from django import forms
from .choices import *
from django.db import models
from django.forms import formset_factory


# from django.forms import ModelForm

class LoanForm(forms.ModelForm):
    class Meta:
        model = Loan
        fields = ('loaner', 'expected_return_date')


class LoanerForm(forms.ModelForm):
    class Meta:
        model = Loaner
        exclude = ['creation_date']


class TypeForm(forms.ModelForm):
    material_type = models.CharField(choices=TYPE_CHOICES, default='generic')

    class Meta:
        model = Type
        fields = '__all__'


class MaterialForm(forms.ModelForm):
    class Meta:
        model = Material
        # fields = '__all__'
        exclude = ['creation_date_mat']


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)
    establishment = forms.CharField(required=True)

    class Meta:
        model = User
        fields = ("username", "email", "establishment", "password1", "password2")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.establishment = self.cleaned_data['establishment']
        if commit:
            user.save()
        return user

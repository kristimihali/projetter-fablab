# Generated by Django 2.2 on 2020-02-27 15:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_material_loan'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='loan',
            field=models.ForeignKey(blank=True, default=1, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='main.LoanMaterial'),
        ),
    ]

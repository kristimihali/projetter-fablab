# Generated by Django 2.2 on 2020-03-01 10:01

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_auto_20200301_1001'),
    ]

    operations = [
        migrations.AlterField(
            model_name='type',
            name='creation_date_type',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]

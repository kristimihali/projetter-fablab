# Generated by Django 2.2 on 2020-02-27 09:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0011_loan_loanmaterial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='loanmaterial',
            name='barcode',
        ),
    ]

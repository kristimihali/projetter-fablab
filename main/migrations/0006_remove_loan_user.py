# Generated by Django 2.2 on 2020-02-27 08:12

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_loan'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='loan',
            name='user',
        ),
    ]

# Generated by Django 2.2 on 2020-02-28 16:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_auto_20200228_1654'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='name',
            field=models.CharField(default='Test', max_length=250),
        ),
    ]

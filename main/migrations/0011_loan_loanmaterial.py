# Generated by Django 2.2 on 2020-02-27 08:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0010_auto_20200227_0843'),
    ]

    operations = [
        migrations.CreateModel(
            name='LoanMaterial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('creation_date_loan_mat', models.DateTimeField(auto_now_add=True)),
                ('barcode', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='main.Material')),
            ],
        ),
        migrations.CreateModel(
            name='Loan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date_loan', models.DateTimeField(verbose_name='Creation of Loan')),
                ('expected_return_date', models.DateField(blank=True)),
                ('return_date', models.DateField(blank=True, null=True)),
                ('materials', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='main.LoanMaterial')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='main.User')),
            ],
        ),
    ]

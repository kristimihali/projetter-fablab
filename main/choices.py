TYPE_GENERIC = 'generic'
TYPE_UNIQUE = 'unique'

TYPE_CHOICES = (
    (TYPE_GENERIC, 'generic'),
    (TYPE_UNIQUE, 'unique'),
)